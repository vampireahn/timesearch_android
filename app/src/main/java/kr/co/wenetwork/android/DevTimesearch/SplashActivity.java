package kr.co.wenetwork.android.DevTimesearch;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class SplashActivity extends AppCompatActivity {
    private static final long WAIT_TIME_MILLIS = 3000; // 3 secs.

    // 대기시간을 감지할 쓰레드
    private Thread worker;

    // 대기시간의 경과후 다음화면으로 이동
    private void letsMoveOnNext() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(SplashActivity.this, MainActivity.class); // 다음화면으로 이동할 대상 액티비티
                startActivity(intent);
                finish();
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saplash);
    }

    @Override
    protected void onResume() {
        super.onResume();

        // 사용자가 스플래시 화면표시중 다음화면으로 이동을 위한 대기 트리거를 동작시킵니다.
        worker = new Thread() {
            @Override
            public void run() {
                try {
                    Thread.sleep(WAIT_TIME_MILLIS);
                    letsMoveOnNext();
                } catch (InterruptedException e) {
                }
            }
        };
        worker.start();
    }

    @Override
    protected void onPause() {
        // 사용자가 스플래시 화면표시중 강제로 닫을 경우 대기 트리거를 제거합니다.
        worker.interrupt();
        super.onPause();
    }
}
